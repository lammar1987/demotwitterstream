package com.lammar.twitterstreamdemo.ui.tweets;

import android.content.Context;

import com.lammar.twitterstreamdemo.data.Tweet;
import com.lammar.twitterstreamdemo.data.source.StreamService;
import com.lammar.twitterstreamdemo.helper.NetworkConnectivityHelper;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Scheduler;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.plugins.RxJavaHooks;
import rx.schedulers.Schedulers;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TweetsPresenterTest {

    @Mock
    private TweetsContract.View tweetsView;

    @Mock
    private Context mockContext;

    private TweetsPresenter tweetsPresenter;

    @Mock
    private NetworkConnectivityHelper networkConnectivityHelper;

    @Mock
    private StreamService streamService;

    @Before
    public void setupTweetsPresenter(){

        MockitoAnnotations.initMocks(this);
        tweetsPresenter = new TweetsPresenter(networkConnectivityHelper, streamService, tweetsView);

        // Override RxJava scheduler
        RxJavaHooks.setOnNewThreadScheduler(scheduler -> Schedulers.immediate());

        // Override RxAndroid schedulers
        final RxAndroidPlugins rxAndroidPlugins = RxAndroidPlugins.getInstance();
        rxAndroidPlugins.registerSchedulersHook(new RxAndroidSchedulersHook() {
            @Override
            public Scheduler getMainThreadScheduler() {
                return Schedulers.immediate();
            }
        });
    }

    @After
    public void tearDown() throws Exception {
        RxJavaHooks.reset();
        RxAndroidPlugins.getInstance().reset();
    }

    @Test
    public void showTweetsWhenNoConnection_ShowsNoConnectionError(){

        //Given that there is no network connection
        when(networkConnectivityHelper.isNetworkAvailable()).thenReturn(false);

        //When connection to the stream is requested
        tweetsPresenter.listenForNewTweets("");

        //Then no network error is shown
        verify(tweetsView).showNoNetworkError();
    }

    @Test
    public void getTweetsWhenReturnedObjectIsNull_ShowGeneralError(){
        //Given that there is a network connection
        when(networkConnectivityHelper.isNetworkAvailable()).thenReturn(true);

        //And null object is returned
        when(streamService.getTweets("")).thenReturn(Observable.just(null));

        //When connection to the stream is requested
        tweetsPresenter.listenForNewTweets("");

        //Then general error is shown
        verify(tweetsView).showGeneralProblemError();
    }

    @Test
    public void getTweets_ShowTweets(){
        //Given that there is a network connection
        when(networkConnectivityHelper.isNetworkAvailable()).thenReturn(true);

        //And tweet object is returned
        Tweet tweet = new Tweet();
        when(streamService.getTweets("")).thenReturn(Observable.just(tweet));

        //When connection to the stream is requested
        tweetsPresenter.listenForNewTweets("");

        //Then tweet is shown
        List<Tweet> items = new ArrayList<>();
        items.add(tweet);
        verify(tweetsView).showTweets(items);
    }
}