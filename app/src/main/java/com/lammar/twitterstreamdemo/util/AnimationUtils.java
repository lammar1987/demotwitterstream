package com.lammar.twitterstreamdemo.util;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

/**
 * Created by marcinlament on 07/02/2017.
 */

public class AnimationUtils {

    public static ObjectAnimator getTranslationZFrameAnimator(View view, int start, int end) {
        ObjectAnimator translationZFrameAnimator = ObjectAnimator.ofFloat (view, "translationZ", start, end);
        translationZFrameAnimator.setDuration(300);
        translationZFrameAnimator.setInterpolator (new DecelerateInterpolator());
        return translationZFrameAnimator;
    }

    public static  ObjectAnimator getTranslationYFrameAnimator(View view, int start, int end) {
        ObjectAnimator translationYFrameAnimator = ObjectAnimator.ofFloat (view, "translationY", start, end);
        translationYFrameAnimator.setDuration(300);
        translationYFrameAnimator.setInterpolator (new DecelerateInterpolator());
        return translationYFrameAnimator;
    }

    public static  ValueAnimator getMainFrameColorAnimation(Context context, View view, int from, int to) {
        int colorFrom = ContextCompat.getColor(context, from);
        int colorTo = ContextCompat.getColor(context, to);
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation.setDuration(300);
        colorAnimation.addUpdateListener(valueAnimator -> DrawableCompat.setTint(view.getBackground().mutate(), (int) valueAnimator.getAnimatedValue()));
        return colorAnimation;
    }
}
