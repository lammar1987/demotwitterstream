package com.lammar.twitterstreamdemo.ui.tweets;

import com.lammar.twitterstreamdemo.TwitterConfig;
import com.lammar.twitterstreamdemo.data.Tweet;
import com.lammar.twitterstreamdemo.data.source.StreamService;
import com.lammar.twitterstreamdemo.helper.NetworkConnectivityHelper;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by marcinlament on 06/02/2017.
 */

public class TweetsPresenter implements TweetsContract.Presenter {

    private NetworkConnectivityHelper networkConnectivityHelper;
    private StreamService streamService;
    private TweetsContract.View view;
    private List<Tweet> tweets;
    private Subscription subscription;

    public TweetsPresenter(NetworkConnectivityHelper networkConnectivityHelper, StreamService streamService, TweetsContract.View view){
        this.networkConnectivityHelper = networkConnectivityHelper;
        this.streamService = streamService;
        this.view = view;
        this.tweets = new ArrayList<>();
        view.setPresenter(this);
    }

    @Override
    public void listenForNewTweets(final String subject) {

        if(view == null) return;

        if(!networkConnectivityHelper.isNetworkAvailable()){
            view.showNoNetworkError();
            return;
        }

        view.showInitializingState();

        Observable<Tweet> observable = streamService.getTweets(subject);

        subscription = observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Tweet>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        if(view != null) {
                            view.showGeneralProblemError();
                            view.showDefaultState();
                        }
                    }

                    @Override
                    public void onNext(Tweet tweet) {
                        if(view == null) return;

                        if(tweet == null){
                            view.showGeneralProblemError();
                            view.showDefaultState();
                            return;
                        }

                        if(tweets.size() == 0){
                            view.showActiveState();
                        }

                        if(tweets.size() == TwitterConfig.MAX_TWEETS_COUNT){
                            tweets.remove(TwitterConfig.MAX_TWEETS_COUNT - 1);
                        }
                        tweets.add(0, tweet);
                        view.showTweets(tweets);
                    }
                });
    }

    @Override
    public void clearTweets() {
        tweets.clear();
        if(view != null){
            view.showTweets(new ArrayList<>());
            view.showDefaultState();
        }
        if(subscription != null){
            subscription.unsubscribe();
            subscription = null;

            streamService.release();
        }
    }

    @Override
    public void initialize() {
        if (view != null) {
            if(subscription == null || subscription.isUnsubscribed()) {
                view.showDefaultState();
            }
        }
    }

    @Override
    public void destroy() {
        tweets.clear();
        view = null;
        if(subscription != null){
            subscription.unsubscribe();
            subscription = null;
            streamService.release();
        }
    }
}
