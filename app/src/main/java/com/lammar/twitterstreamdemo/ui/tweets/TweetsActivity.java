package com.lammar.twitterstreamdemo.ui.tweets;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.lammar.twitterstreamdemo.R;
import com.lammar.twitterstreamdemo.data.source.TwitterStreamService;
import com.lammar.twitterstreamdemo.helper.NetworkConnectivityHelper;
import com.lammar.twitterstreamdemo.util.ActivityUtils;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by marcinlament on 06/02/2017.
 */

public class TweetsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweets);

        TweetsFragment tweetsFragment =  (TweetsFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (tweetsFragment == null) {
            tweetsFragment = TweetsFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), tweetsFragment, R.id.content_frame);
        }

        TwitterStreamService twitterStreamService = new TwitterStreamService();
        NetworkConnectivityHelper networkConnectivity = new NetworkConnectivityHelper(this);

        TweetsPresenter presenter = new TweetsPresenter(networkConnectivity, twitterStreamService, tweetsFragment);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
