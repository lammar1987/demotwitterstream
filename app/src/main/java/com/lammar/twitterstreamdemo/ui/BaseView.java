package com.lammar.twitterstreamdemo.ui;

/**
 * Created by marcinlament on 06/02/2017.
 */

public interface BaseView <T>{

    void setPresenter(T presenter);
}