package com.lammar.twitterstreamdemo.ui.tweets.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lammar.twitterstreamdemo.R;
import com.lammar.twitterstreamdemo.data.Tweet;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by marcinlament on 06/02/2017.
 */

public class TweetsAdapter extends RecyclerView.Adapter<TweetsAdapter.MyViewHolder> {

    private List<Tweet> items;
    private SimpleDateFormat simpleDateFormat;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView userName, date, message;
        public ImageView userImage;

        public MyViewHolder(View view) {
            super(view);
            userName = (TextView) view.findViewById(R.id.list_item_user_name);
            date = (TextView) view.findViewById(R.id.list_item_date);
            message = (TextView) view.findViewById(R.id.list_item_message);
            userImage = (ImageView) view.findViewById(R.id.list_item_user_image);
        }
    }

    public TweetsAdapter(Context context){
        items = new ArrayList<>();
        simpleDateFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_tweet_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Tweet tweet = items.get(position);
        holder.userName.setText(tweet.userName);
        holder.date.setText(simpleDateFormat.format(tweet.created));
        holder.message.setText(tweet.message);

        Picasso.with(context)
                .load(tweet.userImage)
                .into(holder.userImage);
    }

    public void setItems(List<Tweet> items) {
        this.items = items;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}