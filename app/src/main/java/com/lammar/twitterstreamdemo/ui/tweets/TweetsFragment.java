package com.lammar.twitterstreamdemo.ui.tweets;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lammar.twitterstreamdemo.R;
import com.lammar.twitterstreamdemo.TwitterConfig;
import com.lammar.twitterstreamdemo.data.Tweet;
import com.lammar.twitterstreamdemo.ui.tweets.adapter.TweetsAdapter;
import com.lammar.twitterstreamdemo.util.AnimationUtils;
import com.lammar.twitterstreamdemo.util.DialogUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by marcinlament on 06/02/2017.
 */

public class TweetsFragment extends Fragment implements TweetsContract.View {

    private TweetsContract.Presenter presenter;

    @BindView(R.id.subject_input) EditText subjectInput;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.input_frame) View contentFrame;
    @BindView(R.id.start_btn) View startButton;
    @BindView(R.id.stop_btn) View stopButton;
    @BindView(R.id.progress_bar) ProgressBar progressBar;
    @BindView(R.id.intro_message) TextView introMessage;

    private TweetsAdapter adapter;

    public TweetsFragment() {
        // Requires empty public constructor
    }

    public static TweetsFragment newInstance() {
        return new TweetsFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        adapter = new TweetsAdapter(getContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext()) {
            @Override
            public boolean supportsPredictiveItemAnimations() {
                return true;
            }
        };
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        subjectInput.setOnFocusChangeListener((view, hasFocus) -> {
            if(hasFocus){
                startUpAnimation();
            }else{
                startDownAnimation();
            }
        });

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tweets, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.initialize();
    }

    @Override
    public void onDestroy() {
        presenter.destroy();
        super.onDestroy();
    }

    @OnClick(R.id.intro_message)
    public void onIntroMessageClick(View view) {
        if(subjectInput.hasFocus()) {
            subjectInput.clearFocus();
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(subjectInput.getWindowToken(), 0);
        }
    }

    @OnClick(R.id.start_btn)
    public void onStartClick(View view) {
        presenter.listenForNewTweets(subjectInput.getText().toString());
    }

    @OnClick(R.id.stop_btn)
    public void onStopClick(View view) {
        presenter.clearTweets();
    }

    @Override
    public void showDefaultState() {
        introMessage.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        startButton.setVisibility(View.VISIBLE);
        stopButton.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showInitializingState() {
        introMessage.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        startButton.setVisibility(View.GONE);
        stopButton.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        clearSubjectInputFocus();
    }

    @Override
    public void showActiveState() {
        introMessage.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        startButton.setVisibility(View.GONE);
        stopButton.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showTweets(List<Tweet> tweets) {

        adapter.setItems(tweets);

        if(tweets.size() > 0) {
            if (tweets.size() == TwitterConfig.MAX_TWEETS_COUNT) {
                adapter.notifyItemRemoved(TwitterConfig.MAX_TWEETS_COUNT - 1);
            }
            adapter.notifyItemInserted(0);
            recyclerView.scrollToPosition(0);
        }else{
            adapter.notifyDataSetChanged();
        }
    }

    private void clearSubjectInputFocus(){
        if(subjectInput.hasFocus()) {
            subjectInput.clearFocus();
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(subjectInput.getWindowToken(), 0);
        }
    }

    @Override
    public void setPresenter(TweetsContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showNoNetworkError() {
        DialogUtils.showAlertDialog(getActivity(),
                R.string.no_connection_error_title,
                R.string.no_connection_error_message,
                R.string.no_connection_error_button);
    }

    @Override
    public void showGeneralProblemError() {
        DialogUtils.showAlertDialog(getActivity(),
                R.string.general_error_title,
                R.string.general_error_message,
                R.string.general_error_button);
    }

    private void startUpAnimation(){
        contentFrame.clearAnimation();
        ObjectAnimator translationZFrameAnimator = AnimationUtils.getTranslationZFrameAnimator(contentFrame, 0, 10);
        translationZFrameAnimator.start();

        ObjectAnimator translationYFrameAnimator = AnimationUtils.getTranslationYFrameAnimator(contentFrame, 0, 30);
        translationYFrameAnimator.start();

        ValueAnimator colorAnimation = AnimationUtils.getMainFrameColorAnimation(getContext(), contentFrame,
                R.color.colorPrimaryDark, R.color.colorSecondary);
        colorAnimation.start();
    }

    private void startDownAnimation(){
        contentFrame.clearAnimation();
        ObjectAnimator translationZFrameAnimator = AnimationUtils.getTranslationZFrameAnimator(contentFrame, 10, 0);
        translationZFrameAnimator.start();

        ObjectAnimator translationYFrameAnimator = AnimationUtils.getTranslationYFrameAnimator(contentFrame, 30, 0);
        translationYFrameAnimator.start();

        ValueAnimator colorAnimation = AnimationUtils.getMainFrameColorAnimation(getContext(), contentFrame,
                R.color.colorSecondary, R.color.colorPrimaryDark);
        colorAnimation.start();
    }


}
