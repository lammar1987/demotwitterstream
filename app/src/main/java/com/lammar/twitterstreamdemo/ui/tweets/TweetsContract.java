package com.lammar.twitterstreamdemo.ui.tweets;

import com.lammar.twitterstreamdemo.data.Tweet;
import com.lammar.twitterstreamdemo.ui.BasePresenter;
import com.lammar.twitterstreamdemo.ui.BaseView;

import java.util.List;

/**
 * Created by marcinlament on 06/02/2017.
 */

public interface TweetsContract {

    interface View extends BaseView<Presenter> {

        void showNoNetworkError();
        void showGeneralProblemError();

        void showDefaultState();
        void showInitializingState();
        void showActiveState();

        void showTweets(List<Tweet> tweets);
    }

    interface Presenter extends BasePresenter {

        void listenForNewTweets(String subject);
        void clearTweets();
    }
}
