package com.lammar.twitterstreamdemo.ui;

/**
 * Created by marcinlament on 06/02/2017.
 */

public interface BasePresenter {

    void initialize();
    void destroy();
}