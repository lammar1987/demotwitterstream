package com.lammar.twitterstreamdemo.data.source;

import com.lammar.twitterstreamdemo.TwitterConfig;
import com.lammar.twitterstreamdemo.data.Tweet;
import com.lammar.twitterstreamdemo.data.source.StreamService;

import rx.Observable;
import rx.Subscriber;
import twitter4j.FilterQuery;
import twitter4j.Status;
import twitter4j.StatusAdapter;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by mlament on 07/02/2017.
 */

public class TwitterStreamService implements StreamService {


	private final TwitterStream twitterStream;

	public TwitterStreamService(){
		ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		configurationBuilder.setDebugEnabled(true)
				.setOAuthConsumerKey(TwitterConfig.CONSUMER_KEY)
				.setOAuthConsumerSecret(TwitterConfig.CONSUMER_SECRET)
				.setOAuthAccessToken(TwitterConfig.ACCESS_TOKEN)
				.setOAuthAccessTokenSecret(TwitterConfig.TOKEN_SECRET);

		twitterStream = new TwitterStreamFactory(configurationBuilder.build()).getInstance();
	}

	@Override
	public Observable<Tweet> getTweets(String subject) {
		return Observable.create(new Observable.OnSubscribe<Tweet>(){

			@Override
			public void call(final Subscriber<? super Tweet> subscriber) {

				twitterStream.clearListeners();
				twitterStream.addListener(new StatusAdapter(){
					public void onStatus(Status status) {

						Tweet tweet = new Tweet();
						tweet.userName = status.getUser().getName();
						tweet.userImage = status.getUser().getBiggerProfileImageURL();
						tweet.created = status.getCreatedAt();
						tweet.message = status.getText();

						subscriber.onNext(tweet);
					}
					public void onException(Exception ex) {
						subscriber.onError(ex);
					}
				});
				FilterQuery filterQuery = new FilterQuery();
				filterQuery.track(new String[]{subject});
				twitterStream.filter(filterQuery);
			}
		});
	}

	@Override
	public void release() {
		twitterStream.cleanUp();
	}
}
