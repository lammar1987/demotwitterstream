package com.lammar.twitterstreamdemo.data;

import java.util.Date;

/**
 * Created by mlament on 07/02/2017.
 */

public class Tweet {

    public String userName;
    public String userImage;
    public String message;
    public Date created;
}
