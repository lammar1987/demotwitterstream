package com.lammar.twitterstreamdemo.data.source;


import com.lammar.twitterstreamdemo.data.Tweet;

import rx.Observable;

/**
 * Created by mlament on 07/02/2017.
 */

public interface StreamService {

	Observable<Tweet> getTweets(String subject);
	void release();
}
